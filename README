In order to be able to start with the competency test, follow the following steps:

1) Pull this repository from bitbucket
   git clone https://bitbucket.org/dbadenhorst/lifeq_daniel
2) Unzip data.zip in data folder (password given in email)
3) Navigate to the directory in your terminal 
   cd lifeq_experiment
4) Create virtual environment
   virtualenv ./venv
5) Activate virtual environment
   source ./venv/bin/activate
6) Install necessary packages
   pip install -r ./requirements
7) Open notebook from terminal
   jupyter notebook
8) Open read_data.ipynb in browser
9) Start coding

Once the data.zip file is unzipped, you have 10 h5 files in the ./data/ folder. Each of these 10 files represent data for a single individual measured over the course of 7 days.
Each h5 file has the following data streams :
1) Heart rate and heart rate confidence - The heart rate of the individual measured once per second (1Hz) in beats per minute, together with a confidence score (between 0 and 100) indicating the level of certainty in before mentioned heart rate. 
2) Breathing rate and breathing rate confidence - The breathing rate of the individual (measured in 1Hz, only during sleep) in breaths per minute, together with a confidence score (between 0 and 100) indicating the level of certainty in before mentioned breathing rate.
2) Acceleration - A datastream measured at 1Hz indicating the level of activity of the individual. It is merely a relative measurement and needs to be considered on a relative scale.
4) RR interval and associated confidence - The time difference between two consecutive heart beats. This data stream contains a value for each identified heartbeat, consequently it is not measured at a fixed sampling frequency. An associated confidence score (between 0 and 100) is also given indicating the level of certainty in before RR interval.
5) PPG signal - The raw PPG signal (at 25Hz) measured from the wearable device. This signal contains information on blood flow in the capilaries of the individual; this is the signal we use to calculate Heart rate.
3) Sleep stages - A datastream containing start and stop tokens that give the different sleep stages the individual goes through during a night's sleep.
5) Profile information - A matrix containing profile information on the subject. This includes:
    i)    Age
    ii)   Gender
    iii)  Weight
    iv)   Height
    v)    Blood pressure
    vi)   Cholesterol
    vi)   Smoking status
Take note: 
a - Each of these data streams are indexed by UTC timestamps in nano seconds.
b - The corresponding UTC offset is +2 hours for all UTC timestamps.
c - See read_data.ipynb in jupyter notebook for easy access to each of these data streams.

The objective of this exercise is to assign a value to each of these 10 individuals representing how healthy each of them are on a scale from 0 (being unhealthy) to 100 (being healthy).
There is of course no correct answer here. Consequently you will merely be evaluated on how you approach the problem and not on how "accurate" your health scores are. 

Please use the functionality of the notebook to document :
1) Exploration of the data
Plotting and commenting on figures of these individuals' datastreams
2) Data reduction
Creating single values from some of these datastreams to be representative of the individual
Take Note : You do not need to use each of the provided datastreams
3) Creating a single health score for each individual

If you have any technical queries, do not hesitate to contact dirk@healthqtech.com.
